package com.ebuh;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.ImageView;

public class PageFragment extends Fragment {

    private int pageNumber;

    public static PageFragment newInstance(int page) {
        PageFragment fragment = new PageFragment();
        Bundle args=new Bundle();
        args.putInt("num", page);
        fragment.setArguments(args);
        return fragment;
    }

    public PageFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        pageNumber = getArguments() != null ? getArguments().getInt("num") : 1;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View result=inflater.inflate(R.layout.fragment_page, container, false);
        TextView pageHeader=(TextView)result.findViewById(R.id.displayText);
        ImageView img =  (ImageView)result.findViewById(R.id.displayImage);

        switch (pageNumber) {
            case 0: ;img.setImageResource(R.mipmap.pic1);
                break;
            case 1: ;img.setImageResource(R.mipmap.pic2);
                break;
            case 2: ;img.setImageResource(R.mipmap.pic3);
                break;

        }

        //String header = String.format("Фрагмент %d", pageNumber+1);
        //pageHeader.setText(header);
        return result;
    }
}